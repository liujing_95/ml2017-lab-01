import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_svmlight_file
from sklearn.model_selection import train_test_split

# paremeters
iteration = 100
eta = 0.01

def get_data():
    data_path = './housing_scale'
    X, y = load_svmlight_file(data_path)
    X = X.toarray()
    b = np.ones((X.shape[0], 1))
    X = np.concatenate((b, X), axis=1)
    y = np.expand_dims(y, axis=1)
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.3, random_state=42)
    return X_train, X_val, y_train, y_val

X_train, X_val, y_train, y_val = get_data()

def plot(train_loss, val_loss):
    fig, ax = plt.subplots()
    ax.plot(train_loss, label='train', linewidth=2.0)
    ax.plot(val_loss, label='validation', linewidth=2.0)
    ax.set(xlabel='Iteration', ylabel='Loss')
    ax.grid()
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)
    plt.show()

def GD_train(X_train, y_train, X_val, y_val, W):
    train_loss = np.zeros((iteration, ))
    val_loss = np.zeros((iteration, ))
    for i in range(iteration):
        train_loss[i] = 1 / (2.0 * n) * np.dot(np.transpose(np.dot(X_train, W) - y_train), np.dot(X_train, W) - y_train)
        val_loss[i] = 1 / (2.0 * val_n) * np.dot(np.transpose(np.dot(X_val, W) - y_val), np.dot(X_val, W) - y_val)
        W_grad = np.dot(X_train.T, X_train.dot(W) - y_train) / n
        W = W - eta * W_grad
    return train_loss, val_loss

train_loss, val_loss = GD_train(X_train, y_train, X_val, y_val, W)
plot(train_loss, val_loss)
