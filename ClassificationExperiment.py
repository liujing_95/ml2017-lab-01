from sklearn.datasets import load_svmlight_file
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt

iteration = 100
eta = 0.01
lbda = 10

def get_data():
    data_path = './australian_scale'
    X, y = load_svmlight_file(data_path)
    X = X.toarray()
    b = np.ones((X.shape[0], 1))
    X = np.concatenate((b, X), axis=1)
    y = np.expand_dims(y, axis=1)
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.33, random_state=42)
    return X_train, X_val, y_train, y_val

X_train, X_val, y_train, y_val = get_data()

n, dim = X_train.shape
val_n = X_train.shape[0]
W = np.random.randn(dim, 1)


def plot_loss(train_loss, val_loss):
    fig, ax = plt.subplots()
    ax.plot(train_loss, label='train', linewidth=2.0)
    ax.plot(val_loss, label='validation', linewidth=2.0)
    ax.set(xlabel='Iteration', ylabel='Loss')
    ax.grid()
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)
    plt.show()


def plot_error(val_loss):
    fig, ax = plt.subplots()
    ax.plot(val_loss, linewidth=2.0)
    ax.set(xlabel='Iteration', ylabel='Error')
    ax.grid()
    plt.show()

def GD_train(X_train, y_train, X_val, y_val, W):
    train_loss = np.zeros((iteration, ))
    val_loss = np.zeros((iteration, ))
    val_error = np.zeros((iteration, ))
    for i in range(iteration):
        condition = y_train * (X_train.dot(W))
        val_condition = y_val * (X_val.dot(W))
        condition_n = len(np.where(condition <= 1)[0])
        train_loss[i] = np.sum(np.dot(W.T, W) / 2.0 * lbda + np.maximum(0, 1 - condition)) / n
        val_loss[i] = np.sum(np.dot(W.T, W) / 2.0 * lbda + np.maximum(0, 1 - val_condition)) / val_n
        val_error[i] = np.where(val_condition < 0)[0].size / val_n
        W_grad = lbda * n * W - np.sum((y_train[np.where(condition <= 1)[0], :] * X_train[np.where(condition <= 1)[0], :]).T, axis=1, keepdims=True)
        W_grad = W_grad / n
        W = W - eta * W_grad
    return train_loss, val_loss, val_error

train_loss, val_loss, val_error = GD_train(X_train, y_train, X_val, y_val, W)
plot_loss(train_loss, val_loss)
plot_error(val_error)